﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EnterShop : MonoBehaviour
{
    public void EnterShopBillabong()
    {
        SceneManager.LoadScene("ShopBillabong");

    }
    public void EnterShopDoubleStandard()
    {
        SceneManager.LoadScene("ShopDoubleStandard");
    }
    public void EnterShopEarthMusicEcology()
    {
        SceneManager.LoadScene("ShopEarthMusicEcology");
    }
    public void EnterShopHeliopoleg()
    {
        SceneManager.LoadScene("ShopHeliopole");
    }
}
